FROM gradle:7.1.1-jdk11 AS build

WORKDIR /app
COPY . .
RUN gradle build

FROM openjdk:11-jre-slim

WORKDIR /app
COPY --from=build /app/build/libs/demo-0.0.1-SNAPSHOT.jar /app/java.jar

RUN chown -R 1001:1001 /app \
    && chmod -R g=u /app

USER 1001
EXPOSE 8080
CMD ["java", "-jar", "/app/java.jar"]
